// Теоретичне питання

// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

// Об'єкт має властивість prototype, яка також є об'єктом, куди можна зберігаюти методи цього об'єкту або потрібні нам властивості. 
// При створенні на основі нашого об'єкту нових об'єктів, вони наслідують цю властивість prototype.

// Для чого потрібно викликати super() у конструкторі класу-нащадка?

// super() у конструкторі класу-нащадка наслідує потрібні нам значення властивостей з конструктора класу-батька, які ми впишемо у дужках super().

// Завдання

// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    constructor (name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }

    set name(value) {
        this._name = value;
    }
    get name() {
        return this._name;
    }
    set age(value) {
        this._age = value;
    }
    get age() {
        return this._age;
    }
    set salary(value) {
        this._salary = value;
    }
    get salary() {
        return this._salary;
    }
};

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super (name, age, salary)
        this.lang = lang
    }

    get salary() {
        return this._salary * 3;
    }
};

const programmer1 = new Programmer("Uasya", 35, 15000, ["javaScript", "java"]);
console.log(programmer1);
console.log(programmer1.salary);

const programmer2 = new Programmer("Sveta", 28, 17000, ["javaScript", "java"]);
console.log(programmer2);
console.log(programmer2.salary);